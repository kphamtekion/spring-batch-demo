package com.enditall.batch.config;

import com.enditall.batch.student.Student;
import org.springframework.batch.item.ItemProcessor;

public class StudentProcessor implements ItemProcessor<Student, Student> {

    @Override
    public Student process(Student student) throws Exception {
        //blah blah logic
        return student;
    }
}
